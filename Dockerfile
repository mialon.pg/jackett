FROM alpine:3.21.3

# renovate: datasource=github-releases depName=Jackett/Jackett versioning=semver-coerced
ENV JACKETT_VER=0.22.1512

# renovate: datasource=repology depName=alpine_3_21/dumb-init versioning=loose
ENV DUMB_INIT_VERSION="1.2.5-r3"

ENV CFG_DIR=/config

WORKDIR /jackett

# https://github.com/Jackett/Jackett/wiki/Installation-on-Alpine-Linux
RUN set -feux \
  &&  apk --no-cache add dumb-init=${DUMB_INIT_VERSION} \
    bash \
    curl \
    gcompat \
    icu-libs \
    krb5-libs \
    libgcc \
    libintl \
    libssl3 \
    libstdc++ \
    sqlite-libs \
    zlib

RUN set -feux \
  && wget -O- https://github.com/Jackett/Jackett/releases/download/v${JACKETT_VER}/Jackett.Binaries.LinuxMuslAMDx64.tar.gz \
        | tar xz --strip-components=1

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/jackett/jackett", "-x", "-d", "/config", "--NoUpdates"]
